import axios from 'axios';

const endPoint = {
	host : 'localhost',
	port : '9097',
}

const baseUrl = `http://${endPoint.host}:${endPoint.port}`;

const EmployeeService = {

	fetchAllEmployees(sortBy, success, error) {
        axios.get(`${baseUrl}/employees/?sortBy=${sortBy}`).then(response => {
            console.log(response.data.employees);
            success(response.data.employees);
        }).catch(error => {
            console.log(error);
        });
	},

	fetchEmployeeById(id, success, error) {
		axios.get(`${baseUrl}/employees/${id}`).then(response => {
			console.log(response.data);
			success(response.data);
		}).catch(error => {
		   	console.log(error);
		});
	}

}

export default EmployeeService;
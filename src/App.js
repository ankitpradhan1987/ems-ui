import React from 'react';
import './App.css';
import { Container } from 'react-bootstrap';
import { Switch, Route } from 'react-router-dom';
import EmployeesViewContainer from './containers/EmployeesViewContainer';
import SingleEmployeeViewContainer from './containers/SingleEmployeeViewContainer';

function App() {

    return (
        <div className="App">
            


            <Container style={{marginTop: 30}}>
                <Switch>
                    <Route exact path='/' component={EmployeesViewContainer}/>
                    <Route exact path='/employees/:id' component={SingleEmployeeViewContainer}/>
                </Switch>
            </Container>
        </div>
    );
}

export default App;

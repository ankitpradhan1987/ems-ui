import React, { Component } from 'react';
import { Alert, Spinner } from 'react-bootstrap';
import EmployeeTable from '../components/EmployeeTable';
import employeeService from '../services/EmployeeService';


export default class EmployeesViewContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            employees: [],
            loading: true
        }

        this.sort = this.sort.bind(this);
        this.fetchEmployees = this.fetchEmployees.bind(this);
    }

    componentDidMount() {

        this.fetchEmployees(null);
    }

    sort(key) {
        
        this.setState({
            loading: true
        }, () => {
            this.fetchEmployees(key);
        })
        
    }

    fetchEmployees(sortKey) {
        
        employeeService.fetchAllEmployees(sortKey, (employees) => {
            this.setState({
                employees: employees,
                loading: false
            })
        }, (error) => {
            
        });
    }

    render() {

        return (
            <div>
                {this.state.loading && 
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                }
                {!this.state.loading && 
                    <div>
                        {this.state.employees.length === 0 && 
                            <Alert variant="danger">No Employees Found</Alert>
                        }
                        {this.state.employees.length > 0 && 
                            <EmployeeTable employees={this.state.employees} sortAction={this.sort}/>
                        }
                    </div>
                }
                
            </div>);

    }

}
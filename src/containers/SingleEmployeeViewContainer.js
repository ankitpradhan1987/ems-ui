import React, { Component } from 'react';
import { Alert, Spinner } from 'react-bootstrap';
import SingleEmployeeView from '../components/SingleEmployeeView';
import employeeService from '../services/EmployeeService';


export default class SingleEmployeeViewContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true
        }
    }

    componentDidMount() {

        this.fetchEmployee(this.props.match.params.id);
    }

    fetchEmployee(id) {
        
        employeeService.fetchEmployeeById(id, (employee) => {
            this.setState({
                loading: false,
                employee: employee
            })
        }, (error) => {

        });
    }

    render() {

        return (
            <div>
                {this.state.loading && 
                    <Spinner animation="border" role="status">
                        <span className="sr-only">Loading...</span>
                    </Spinner>
                }
                {!this.state.loading && 
                    <div>
                        {!this.state.employee && 
                            <Alert variant="danger">No Employee Found</Alert>
                        }
                        {this.state.employee && 
                            <SingleEmployeeView employee={this.state.employee} />
                        }
                    </div>
                }
                
            </div>);

    }

}
import React, { Component } from 'react';
import { Table } from 'react-bootstrap';


export default class SortActionTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: props.data
        }
    }

    render() {

        const caretStyle = {
            position: 'absolute',
            right: '20px',
            fontSize: '1.5em',
            fontWeight: 'bolder',
            color: 'gray',
            cursor: 'pointer'
        }

        return (
            <Table striped bordered hover>
                <thead>
                    <tr>
                        {this.props.fields.map(field => {

                            let position = field.sortable? "relative": "static";
                            return (
                                <th key={`header-${field.key}`} style={{position: position}}>{field.label}
                                    {field.sortable && 
                                        <span style={caretStyle} onClick={() => {this.sort(field.key)}}> 
                                            ^ 
                                        </span>
                                    }
                                </th>
                            )
                        })}
                    </tr>
                </thead>
                <tbody>
                    {this.state.rows.map((row, i) => {
                        return (
                            <tr key={`row-${i}`}>
                                {this.props.fields.map(field => {
                                    return (<td key={`row-${i}-field-${field.key}`}>{row[field.key]}</td>)
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
        )

    }

}
import React from 'react';
//import SortActionTable from './utils/SortActionTable';
import { Table } from 'react-bootstrap';
import { Link } from 'react-router-dom';


function EmployeeTable(props) {

	const employeeFields = [{
		'label': 'Employee Id',
		'key': 'employeeId',
		'sortable': false
	}, {
		'label': 'First Name',
		'key': 'firstName',
		'sortable': true,
		'renderer': (emp) => {
			return (<Link to={`/employees/${emp.employeeId}`}>{emp.firstName}</Link>);
		}
	}, {
		'label': 'Last Name',
		'key': 'lastName',
		'sortable': true
	}, {
		'label': 'Contact Number',
		'key': 'phoneNumber',
		'sortable': false
	}, {
        'label': 'Hire Date',
        'key': 'hireDate',
        'sortable': true,
        'renderer': (emp) => new Date(emp.hireDate).toLocaleDateString()+ " T "+new Date(emp.hireDate).toLocaleTimeString()
    }, {
		'label': 'Salary',
		'key': 'salary',
		'sortable': true
	}];

	const caretStyle = {
        position: 'absolute',
        right: '5px',
        fontSize: '1.5em',
        fontWeight: 'bolder',
        color: 'gray',
        cursor: 'pointer'
    }

    return (
    	<Table striped bordered hover>
                <thead>
                    <tr>
                        {employeeFields.map(field => {

                            let position = field.sortable? "relative": "static";
                            return (
                                <th key={`header-${field.key}`} style={{position: position}}>{field.label}
                                    {field.sortable && 
                                        <span style={caretStyle} 
                                              onClick={() => {props.sortAction(field.key)}}> 
                                            ^ 
                                        </span>
                                    }
                                </th>
                            )
                        })}
                    </tr>
                </thead>
                <tbody>
                    {props.employees.map((row, i) => {
                        return (
                            <tr key={`row-${i}`}>
                                {employeeFields.map(field => {
                                    return (<td key={`row-${i}-field-${field.key}`}>
                                    	{field.renderer && field.renderer(row)}
                                    	{!field.renderer && row[field.key]}
                                    	</td>)
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </Table>
  	)
}

export default EmployeeTable;
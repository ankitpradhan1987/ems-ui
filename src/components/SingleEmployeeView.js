import React from 'react';
import { Card, Table } from 'react-bootstrap';

function SingleEmployeeView(props) {

	const emp = props.employee;

    return (
    	<div>
    	
	    	<header><h3>{emp.firstName}  {emp.lastName}</h3> since {new Date(emp.hireDate).toLocaleDateString()+ " T "+new Date(emp.hireDate).toLocaleTimeString()}</header>
			<Table bordered>
			    <tbody>
					<tr>
						<td>Employee Id</td>
						<td>{emp.employeeId}</td>
					</tr>
					<tr>
						<td>First Name</td>
						<td>{emp.firstName}</td>
					</tr>
					<tr>
						<td>Last Name</td>
						<td>{emp.lastName}</td>
					</tr>
					<tr>
						<td>Contact Number</td>
						<td>{emp.phoneNumber}</td>
					</tr>
					<tr>
						<td>Hire Date</td>
						<td>{emp.hireDate}</td>
					</tr>
					<tr>
						<td>Salary</td>
						<td>{emp.salary}</td>
					</tr>
				</tbody>
			</Table>
		</div>
  	)
}

export default SingleEmployeeView;